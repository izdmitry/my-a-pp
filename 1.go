package main

import (
    "github.com/gin-gonic/gin"
    "io/ioutil"
    "log"
    "net/http"
    "encoding/json"
    "time"
    //"fmt"
)

var l , err = time.LoadLocation("Europe/Vienna")
var t = time.Date(2000, 1, 1, 0, 0, 0, 0, l)
var zxc = [5]time.Time{t, t, t, t, t}
var s = [5][]byte{}
var zx = [5][2]string{{"55.755773", "37.617761"},
                    {"59.938806", "30.314278"}, 
                    {"55.795793", "49.106585"}, 
                    {"62.027833", "129.704151"}}

func thisQuestionIsVeryEasyToGoogle(newValue time.Time , ty int) {
    zxc[ty] = newValue
}

func a(ty int) (map[string]interface{}) {
    var dat map[string]interface{}
    log.Println(time.Since(zxc[ty]))
    if time.Since(zxc[ty]) > time.Duration(time.Hour * 2) {
        url := "https://api.weather.yandex.ru/v2/informers?" + "lat=" + zx[ty][0] + "&lon=" + zx[ty][1]
        var bearer = "b4be9531-59f5-4432-ad63-9b5d8ea860ad"
        req, err := http.NewRequest("GET", url, nil)
        req.Header.Add("X-Yandex-API-Key", bearer)
        client := &http.Client{}
        resp, err := client.Do(req)
        //log.Println(req)
        if err != nil {
            log.Println("Error on response.\n[ERROR] -", err)
        }
        defer resp.Body.Close()

        body, err := ioutil.ReadAll(resp.Body)
        if err != nil {
            log.Println("Error while reading the response bytes:", err)
        }
        thisQuestionIsVeryEasyToGoogle(time.Now(), ty)
        s[ty] = body
        json.Unmarshal([]byte(s[ty]), &dat)
        return dat
    }
    json.Unmarshal([]byte(s[ty]), &dat)
    return dat
}

func main() {
    r := gin.Default()

    r.GET("/json", func(c *gin.Context) {
        c.JSON(200, a(0))
    })
    
    r.GET("/json1", func(c *gin.Context) {
        c.JSON(200, a(1))
    })

    r.GET("/json2", func(c *gin.Context) {
        c.JSON(200, a(2))
    })

    r.GET("/json3", func(c *gin.Context) {
        c.JSON(200, a(3))
    })

    r.GET("/json4", func(c *gin.Context) {
        zx[4][0] = string(c.DefaultQuery("lat", zx[0][0]))
        zx[4][1] = string(c.DefaultQuery("lon", zx[0][1]))
        zxc[4] = time.Date(2000, 1, 1, 0, 0, 0, 0, l)
        c.JSON(200, a(4))
    })

    // Serves literal characters
    r.GET("/purejson", func(c *gin.Context) {
        c.PureJSON(200, gin.H{
            "html": "Hello, world!",
        })
    })
    
    // listen and serve on 0.0.0.0:8080
    r.Run(":8080")
}
