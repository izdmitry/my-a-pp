package service

import (
	"errors"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(todo model.Todo) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
	UpdateTodo(todo model.Todo) error
}

type TaskService struct {
	Repository repo.TaskRepository
}

func NewTaskService(filePath string) *TaskService {
	return &TaskService{Repository: repo.NewFileTaskRepository(filePath)}
}

func (s *TaskService) ListTodos() ([]model.Todo, error) {
	tasks, err := s.Repository.GetTasks()
	if err != nil {
		return nil, err
	}
	todos := make([]model.Todo, 0, len(tasks))

	for _, v := range tasks {
		todos = append(todos, model.Todo{
			ID:          v.ID,
			Title:       v.Title,
			Description: v.Description,
			Status:      v.Status,
		})
	}
	return todos, nil
}

func (s *TaskService) CreateTodo(todo model.Todo) error {
	task := repo.Task{
		ID:          todo.ID,
		Title:       todo.Title,
		Description: todo.Description,
		Status:      todo.Status,
	}
	err := s.Repository.CreateTask(task)
	if err != nil {
		return err
	}

	return nil
}

func (s *TaskService) CompleteTodo(todo model.Todo) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}
	check := false
	for _, v := range todos {
		if v == todo {
			check = true
			break
		}
	}
	if !check {
		return errors.New("todo to complete not found")
	}

	// some Job
	todo.Status = "Done"

	task := repo.Task{
		ID:          todo.ID,
		Title:       todo.Title,
		Description: todo.Description,
		Status:      todo.Status,
	}
	if err := s.Repository.UpdateTask(task); err != nil {
		return err
	}
	return nil
}

func (s *TaskService) RemoveTodo(todo model.Todo) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}
	check := false
	for _, v := range todos {
		if v == todo {
			check = true
			break
		}
	}
	if !check {
		return errors.New("todo to remove not found")
	}
	if err := s.Repository.DeleteTask(todo.ID); err != nil {
		return err
	}
	return nil

}

func (s *TaskService) UpdateTodo(todo model.Todo) error {
	task := repo.Task{
		ID:          todo.ID,
		Title:       todo.Title,
		Description: todo.Description,
		Status:      todo.Status,
	}
	err := s.Repository.UpdateTask(task)
	if err != nil {
		return err
	}
	return nil
}
