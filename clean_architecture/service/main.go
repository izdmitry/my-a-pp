package main

import (
	"fmt"
	"os"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/model"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/service"
)

func init() {
	file, err := os.OpenFile("./service.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	file.Close()
}

func main() {

	todoRep := service.TodoService(&service.TaskService{Repository: repo.NewFileTaskRepository("./service.json")})

	err := todoRep.CreateTodo(model.Todo{
		ID:          0,
		Title:       "title 1",
		Description: "some description",
		Status:      "some status",
	})
	if err != nil {
		panic(err)
	}
	err = todoRep.CreateTodo(model.Todo{
		ID:          0,
		Title:       "title 2",
		Description: "some description",
		Status:      "some status",
	})
	if err != nil {
		panic(err)
	}
	err = todoRep.CreateTodo(model.Todo{
		ID:          0,
		Title:       "title 3",
		Description: "some description",
		Status:      "some status",
	})
	if err != nil {
		panic(err)
	}

	models, _ := todoRep.ListTodos()

	for _, v := range models {
		fmt.Println(v)
	}
	fmt.Println()

	err = todoRep.CompleteTodo(model.Todo{
		ID:          2,
		Title:       "title 2",
		Description: "some description",
		Status:      "some status",
	})
	if err != nil {
		panic(err)
	}

	models, _ = todoRep.ListTodos()
	for _, v := range models {
		fmt.Println(v)
	}
	fmt.Println()

	err = todoRep.RemoveTodo(model.Todo{
		ID:          2,
		Title:       "title 2",
		Description: "some description",
		Status:      "Done",
	})
	if err != nil {
		panic(err)
	}

	models, _ = todoRep.ListTodos()
	for _, v := range models {
		fmt.Println(v)
	}
	fmt.Println()

	err = todoRep.UpdateTodo(model.Todo{
		ID:          2,
		Title:       "new title",
		Description: "new description",
		Status:      "new status",
	})
	if err != nil {
		panic(err)
	}

	models, _ = todoRep.ListTodos()
	for _, v := range models {
		fmt.Println(v)
	}

}
