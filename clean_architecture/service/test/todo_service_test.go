package test

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/service"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/repo"
)

func Test_taskService_CompleteTodo(t *testing.T) {
	file, err := os.OpenFile("./completeServiceTest.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	_ = file.Close()
	type fields struct {
		repository repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "empty repo",
			fields: fields{repository: repo.NewFileTaskRepository("./completeServiceTest.json")},
			args: args{model.Todo{
				ID:          4,
				Title:       "some title",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: true,
		},
		{
			name:   "correct todo",
			fields: fields{repository: repo.NewFileTaskRepository("./completeServiceTest.json")},
			args: args{model.Todo{
				ID:          4,
				Title:       "some title",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: false,
		},
		{
			name:    "incorrect todo",
			fields:  fields{repository: repo.NewFileTaskRepository("./completeServiceTest.json")},
			args:    args{model.Todo{ID: 1}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TaskService{
				Repository: tt.fields.repository,
			}
			if err := s.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
			{
				file, err := os.OpenFile("./completeServiceTest.json", os.O_WRONLY, 0755)
				if err != nil {
					panic(err)
				}
				err = file.Truncate(0)
				if err != nil {
					panic(err)
				}
				_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 3,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 4,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 5,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 6,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
				if err != nil {
					panic(err)
				}
				_ = file.Close()
			}
		})
	}
}

func TestTaskService_CreateTodo(t *testing.T) {
	file, err := os.OpenFile("./createServiceTest.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	_ = file.Close()

	type fields struct {
		repository repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "empty repo",
			fields: fields{repository: repo.NewFileTaskRepository("./createServiceTest.json")},
			args: args{model.Todo{
				ID:          1,
				Title:       "title 1",
				Description: "description 1",
				Status:      "status 1",
			}},
			wantErr: false,
		},
		{
			name:   "filled repo",
			fields: fields{repository: repo.NewFileTaskRepository("./createServiceTest.json")},
			args: args{model.Todo{
				ID:          2,
				Title:       "title 2",
				Description: "description 2",
				Status:      "status 2",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TaskService{
				Repository: tt.fields.repository,
			}
			if err := s.CreateTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_taskService_ListTodos(t *testing.T) {
	file, err := os.OpenFile("./listServiceTest.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	_ = file.Close()

	type fields struct {
		repository repo.TaskRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		{
			name:    "empty repo",
			fields:  fields{repository: repo.NewFileTaskRepository("./listServiceTest.json")},
			want:    []model.Todo{},
			wantErr: false,
		},
		{
			name:   "filled repo",
			fields: fields{repository: repo.NewFileTaskRepository("./listServiceTest.json")},
			want: []model.Todo{
				{
					ID:          1,
					Title:       "some title",
					Description: "some description",
					Status:      "some status",
				},
				{
					ID:          2,
					Title:       "some title",
					Description: "some description",
					Status:      "some status",
				},
				{
					ID:          3,
					Title:       "some title",
					Description: "some description",
					Status:      "some status",
				},
				{
					ID:          4,
					Title:       "some title",
					Description: "some description",
					Status:      "some status",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TaskService{
				Repository: tt.fields.repository,
			}
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos() got = %v, want %v", got, tt.want)
			}

			file, err := os.OpenFile("./listServiceTest.json", os.O_WRONLY, 0755)
			if err != nil {
				panic(err)
			}
			err = file.Truncate(0)
			if err != nil {
				panic(err)
			}
			_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 3,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 4,\n    \"title\": \"some title\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
			if err != nil {
				panic(err)
			}
			_ = file.Close()
		})
	}
}

func Test_taskService_RemoveTodo(t *testing.T) {
	file, err := os.OpenFile("./removeServiceTest.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title 1\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title 2\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
	if err != nil {
		panic(err)
	}
	_ = file.Close()
	type fields struct {
		repository repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "filled repo, incorrect todo",
			fields: fields{repository: repo.NewFileTaskRepository("./removeServiceTest.json")},
			args: args{model.Todo{
				ID:          1,
				Title:       "some title 2",
				Description: "description",
				Status:      "some status",
			}},
			wantErr: true,
		},
		{
			name:   "filled repo 1",
			fields: fields{repository: repo.NewFileTaskRepository("./removeServiceTest.json")},
			args: args{model.Todo{
				ID:          1,
				Title:       "some title 1",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: false,
		},
		{
			name:   "filled repo 2",
			fields: fields{repository: repo.NewFileTaskRepository("./removeServiceTest.json")},
			args: args{model.Todo{
				ID:          1,
				Title:       "some title 2",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: false,
		},
		{
			name:   "empty repo",
			fields: fields{repository: repo.NewFileTaskRepository("./removeServiceTest.json")},
			args: args{model.Todo{
				ID:          1,
				Title:       "some title 1",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TaskService{
				Repository: tt.fields.repository,
			}
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}

		})
	}
}

func TestTaskService_UpdateTodo(t *testing.T) {
	file, err := os.OpenFile("./updateServiceTest.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}
	_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title 1\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title 2\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
	if err != nil {
		panic(err)
	}
	_ = file.Close()

	type fields struct {
		Repository repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "correct task",
			fields: fields{Repository: repo.NewFileTaskRepository("./updateServiceTest.json")},
			args: args{model.Todo{
				ID:          2,
				Title:       "new title",
				Description: "new description",
				Status:      "new status",
			}},
			wantErr: false,
		},
		{
			name:   "incorrect task",
			fields: fields{Repository: repo.NewFileTaskRepository("/updateServiceTest.json")},
			args: args{model.Todo{
				ID:          3,
				Title:       "new title",
				Description: "new description",
				Status:      "new status",
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TaskService{
				Repository: tt.fields.Repository,
			}
			if err := s.UpdateTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("UpdateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
