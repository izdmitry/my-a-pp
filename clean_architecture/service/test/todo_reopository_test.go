package test

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/repo"
)

func TestFileTaskRepository_CreateTask(t *testing.T) {
	{
		file, err := os.OpenFile("./createRepoTest.json", os.O_RDWR, 0755)
		if err != nil {
			panic(err)
		}
		err = file.Truncate(0)
		if err != nil {
			panic(err)
		}
		_ = file.Close()
	}
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "correct path",
			fields: fields{FilePath: "./createRepoTest.json"},
			args: args{task: repo.Task{
				ID:          0,
				Title:       "some title",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: false,
		},
		{
			name:   "incorrect path",
			fields: fields{FilePath: ""},
			args: args{task: repo.Task{
				ID:          0,
				Title:       "some title",
				Description: "some description",
				Status:      "some status",
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.CreateTask(tt.args.task); (err != nil) != tt.wantErr {
				t.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	{
		file, err := os.OpenFile("./deleteRepoTest.json", os.O_WRONLY, 0755)
		if err != nil {
			panic(err)
		}
		err = file.Truncate(0)
		if err != nil {
			panic(err)
		}
		_ = file.Close()
	}

	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	testTask := repo.Task{
		ID:          0,
		Title:       "some title",
		Description: "some description",
		Status:      "some status",
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "empty repo",
			fields:  fields{FilePath: "./deleteRepoTest.json"},
			args:    args{1},
			wantErr: true,
		},
		{
			name:    "filled repo",
			fields:  fields{FilePath: "./deleteRepoTest.json"},
			args:    args{1},
			wantErr: false,
		},
		{
			name:    "incorrect ID",
			fields:  fields{FilePath: "./deleteRepoTest.json"},
			args:    args{10},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.DeleteTask(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
			_ = repo.CreateTask(testTask)
		})
	}
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	{
		file, err := os.OpenFile("./getTaskRepoTest.json", os.O_WRONLY, 0755)
		if err != nil {
			panic(err)
		}
		err = file.Truncate(0)
		if err != nil {
			panic(err)
		}
		_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title 1\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title 2\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 3,\n    \"title\": \"some title 3\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
		if err != nil {
			panic(err)
		}
		_ = file.Close()
	}

	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		{
			name:   "correct id",
			fields: fields{"getTaskRepoTest.json"},
			args:   args{2},
			want: repo.Task{
				ID:          2,
				Title:       "some title 2",
				Description: "some description",
				Status:      "some status",
			},
			wantErr: false,
		},
		{
			name:    "incorrect id",
			fields:  fields{"getTaskRepoTest.json"},
			args:    args{10},
			want:    repo.Task{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTask(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GerTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GerTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_GetTasks(t *testing.T) {

	file, err := os.OpenFile("./getTaskRepoTest.json", os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title 1\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title 2\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 3,\n    \"title\": \"some title 3\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
	if err != nil {
		panic(err)
	}
	defer file.Close()

	type fields struct {
		FilePath string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []repo.Task
		wantErr bool
	}{
		{
			name:   "filled repository",
			fields: fields{"getTaskRepoTest.json"},
			want: []repo.Task{{
				ID:          1,
				Title:       "some title 1",
				Description: "some description",
				Status:      "some status",
			},
				{
					ID:          2,
					Title:       "some title 2",
					Description: "some description",
					Status:      "some status",
				},
				{
					ID:          3,
					Title:       "some title 3",
					Description: "some description",
					Status:      "some status",
				},
			},
			wantErr: false,
		},
		{
			name:    "empty repo",
			fields:  fields{"getTaskRepoTest.json"},
			want:    []repo.Task{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks() got = %v, want %v", got, tt.want)
			}
			err = file.Truncate(0)
			if err != nil {
				panic(err)
			}
		})
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	{
		file, err := os.OpenFile("./UpdateRepoTest.json", os.O_WRONLY, 0755)
		if err != nil {
			panic(err)
		}
		_, err = file.Write([]byte("[\n  {\n    \"id\": 1,\n    \"title\": \"some title 1\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 2,\n    \"title\": \"some title 2\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  },\n  {\n    \"id\": 3,\n    \"title\": \"some title 3\",\n    \"description\": \"some description\",\n    \"status\": \"some status\"\n  }\n]"))
		if err != nil {
			panic(err)
		}
		_ = file.Close()
	}
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "correct task",
			fields: fields{"./UpdateRepoTest.json"},
			args: args{repo.Task{
				ID:          2,
				Title:       "new title",
				Description: "new description",
				Status:      "new status",
			}},
			wantErr: false,
		},
		{
			name:   "incorrect task",
			fields: fields{"./UpdateRepoTest.json"},
			args: args{repo.Task{
				ID:          10,
				Title:       "new title",
				Description: "new description",
				Status:      "new status",
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.UpdateTask(tt.args.task); (err != nil) != tt.wantErr {
				t.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
