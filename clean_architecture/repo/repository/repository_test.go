package repository

import (
	"io"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		record interface{}
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "save to empty repo",
			fields: fields{},
			args: args{User{
				ID:   0,
				Name: "admin",
			}},
			wantErr: false,
		},
		{
			name:   "save to not empty repo",
			fields: fields{},
			args: args{User{
				ID:   1,
				Name: "John",
			}},
			wantErr: false,
		},
		{
			name:    "incorrect argument",
			fields:  fields{},
			args:    args{"some data"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			file, err := os.OpenFile("./toSave.JSON", os.O_RDWR, 0755)
			if err != nil {
				panic(err)
			}
			defer file.Close()
			r := &UserRepository{
				File: file,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		id int
	}
	file1, err := os.OpenFile("./toFind.JSON", os.O_RDONLY, 0555)
	if err != nil {
		panic(err)
	}
	defer file1.Close()
	file2, err := os.OpenFile("./empty.JSON", os.O_RDONLY, 0555)
	if err != nil {
		panic(err)
	}
	defer file2.Close()
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name:   "correct id",
			fields: fields{file1},
			args:   args{1},
			want: User{
				ID:   1,
				Name: "Liam",
			},
			wantErr: false,
		},
		{
			name:    "incorrect id",
			fields:  fields{file1},
			args:    args{10},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "empty repository",
			fields:  fields{file2},
			args:    args{1},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	type fields struct {
		File io.ReadWriteSeeker
	}
	file1, err := os.OpenFile("./toFind.JSON", os.O_RDONLY, 0555)
	if err != nil {
		panic(err)
	}
	defer file1.Close()
	file2, err := os.OpenFile("./empty.JSON", os.O_RDONLY, 0555)
	if err != nil {
		panic(err)
	}
	defer file2.Close()
	tests := []struct {
		name    string
		fields  fields
		want    interface{}
		wantErr bool
	}{
		{
			name:   "filled repository",
			fields: fields{file1},
			want: []User{
				{
					ID:   0,
					Name: "admin",
				},
				{
					ID:   1,
					Name: "Liam",
				},
				{
					ID:   2,
					Name: "Noah",
				},
			},
			wantErr: false,
		},
		{
			name:    "empty repository",
			fields:  fields{file2},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}
