package repository

import (
	"encoding/json"
	"errors"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriteSeeker
}

func NewUserRepository(file io.ReadWriteSeeker) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (r *UserRepository) Save(record interface{}) error {
	if r.File == nil {
		return errors.New("save file not defined")
	}

	var newData User
	switch val := record.(type) {
	case User:
		newData = val
	default:
		return errors.New("unsupported input data type")
	}

	toUnmarshal, err := io.ReadAll(r.File)
	if err != nil {
		return err
	}

	var toMarshal []User
	if len(toUnmarshal) > 0 {
		err := json.Unmarshal(toUnmarshal, &toMarshal)

		if err != nil {
			return err
		}
	}

	toMarshal = append(toMarshal, newData)
	out, err := json.MarshalIndent(toMarshal, "", " ")
	if err != nil {
		return err
	}
	_, err = r.File.Seek(0, 0)
	if err != nil {
		return err
	}
	_, err = r.File.Write(out)
	if err != nil {
		return err
	}
	_, err = r.File.Seek(0, 0)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	toMarshal, err := io.ReadAll(r.File)
	if err != nil {
		return nil, err
	}
	var data []User
	err = json.Unmarshal(toMarshal, &data)
	if err != nil {
		return nil, err
	}
	if id >= len(data) || id < 0 {
		return nil, errors.New("id out of range")
	}
	if len(data) < 1 {
		return nil, errors.New("user repository is empty")
	}
	var out User
	for _, v := range data {
		if id == v.ID {
			out = v
			break
		}
	}
	_, err = r.File.Seek(0, 0)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (r *UserRepository) FindAll() (interface{}, error) {
	toMarshal, err := io.ReadAll(r.File)
	if err != nil {
		return nil, err
	}
	var out []User
	err = json.Unmarshal(toMarshal, &out)

	if err != nil {
		return nil, err
	}

	if len(out) < 1 {
		return nil, errors.New("user repository is empty")
	}
	_, err = r.File.Seek(0, 0)
	if err != nil {
		return nil, err
	}
	return out, nil
}
