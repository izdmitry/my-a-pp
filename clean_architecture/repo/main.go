package main

import (
	"fmt"
	"os"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/repo/repository"
)

func main() {
	file, err := os.OpenFile("./repo.JSON", os.O_RDWR, 0755)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	repo := repository.NewUserRepository(file)

	user1 := repository.User{
		ID:   0,
		Name: "John",
	}
	user2 := repository.User{
		ID:   1,
		Name: "Max",
	}
	err = repo.Save(user1)
	if err != nil {
		panic(err)
	}

	fmt.Println(repo.FindAll())
	err = repo.Save(user2)
	if err != nil {
		panic(err)
	}
	fmt.Println(repo.Find(0))
	fmt.Println(repo.FindAll())

}
